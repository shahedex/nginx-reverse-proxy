FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY ts4u-fin.crt /usr/share/nginx/
COPY ts4u-new.key /usr/share/nginx/
COPY .htpasswd /usr/share/nginx/
RUN mkdir -p /usr/share/nginx/www/.well-known
COPY apple-developer-merchantid-domain-association /usr/share/nginx/www/.well-known/apple-developer-merchantid-domain-association
# WORKDIR /usr/share/nginx/html
EXPOSE 80
EXPOSE 443

CMD [ "nginx", "-g daemon off;" ]

